import { initObject } from "@neb-sports/mysamay-common-utils";

export class Token {
    userId: string = null;
    clientId: string = null;
    deviceId: string = null;
    roles: string[] = null;
    exp: number = null;

    constructor(data: Partial<Token>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                this[key] = data[key];
            }
            else {
                delete this[key];
            }
        });
    }

}