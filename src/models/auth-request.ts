import { MetadataHelper } from "@neb-sports/mysamay-common-utils";

const dateFields = [
    "createdTime",
    "updatedTime"
]

export class AuthRequest {
    _id: string = null;
    userId: string = null;
    externalUserId: string = null;
    partner: string = null;
    clientId: string = null;
    authURL: string = null;
    callbackURL: string = null;
    token: string = null;
    tokenSecret: string = null;
    userToken: string = null;
    userTokenSecret: string = null;
    expiresAt: number = null;
    refreshToken: string = null;
    nonce: string = null;
    state: string = null;
    timestamp: number = null;
    deleted: boolean = false;
    createdTime: Date = null;
    updatedTime: Date = null;

    constructor(data: Partial<AuthRequest>) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Auth_AuthRequest";

}