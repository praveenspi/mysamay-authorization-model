import { MetadataHelper } from "@neb-sports/mysamay-common-utils";

const dateFields = [
    "createdTime",
    "updatedTime"
]


export class VerificationCode {
    _id: string;
    verificationMedium: string;
    userId: string;
    code: string;
    exp: Date;
    createdTime: Date;
    updatedTime: Date;

    constructor(data: Partial<VerificationCode>) {
        Object.keys(this).forEach(key => {
            if(dateFields.includes(key)) {
                this[key] = new Date(data[key]);
            }
            else if (key in data) {
                this[key] = data[key];
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }



    static collectionName = "Verification_VerificationCode"
}