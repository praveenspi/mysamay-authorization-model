import { MetadataHelper } from "@neb-sports/mysamay-common-utils";

const dateFields = [
    "createdTime",
    "updatedTime"
]

export class Partner {
    _id: string = null;
    partnerName: string = null;
    clientId: string = null;
    clientSecret: string = null;
    authURL: string = null;
    tokenURL: string = null;
    requestTokenURL: string = null;
    userinfoURL: string = null;
    disconnectURL: string = null;
    logoURL: string = null;
    active: boolean = false;
    deactivatedBy: string = null;
    disconnect: boolean = false;
    createdTime: Date = null;
    updatedTime: Date = null;

    constructor(data: Partial<Partner>) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Auth_Partners";

}