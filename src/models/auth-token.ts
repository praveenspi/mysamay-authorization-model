import { MetadataHelper } from "@neb-sports/mysamay-common-utils";
import { Token } from "./token";

const dateFields = [
    "createdTime",
    "updatedTime"
]

export class AuthToken {
    _id: string = null;
    token: Token = null;
    tokenHash: string = null;
    createdTime: Date = null;
    updatedTime: Date = null;

    constructor(data: Partial<AuthToken>) {
        Object.keys(this).forEach(key => {
            if(dateFields.includes(key)) {
                this[key] = new Date(data[key]);
            }
            else if (key in data) {
                this[key] = data[key];
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Auth_UserToken";

}